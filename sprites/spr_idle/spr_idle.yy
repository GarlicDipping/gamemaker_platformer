{
    "id": "70bffa18-871f-4671-8f27-e737ca63e638",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 37,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a16a2598-80b1-4107-9069-0d15d84eddd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "4c5283cb-8f8c-416d-ac3c-9b57aac7a529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16a2598-80b1-4107-9069-0d15d84eddd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57857547-1a92-4db8-9001-f20434dcfcf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16a2598-80b1-4107-9069-0d15d84eddd5",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        },
        {
            "id": "048d7b11-cd15-4240-9e57-89260018f3a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "4acb713f-f556-4adc-b4cf-172c044a93b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048d7b11-cd15-4240-9e57-89260018f3a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae930dbf-33bb-48bb-b74b-7d04e950ed91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048d7b11-cd15-4240-9e57-89260018f3a8",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        },
        {
            "id": "2c728289-406a-4c1e-b9c0-ed46fb6023a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "6ac0caaf-1c15-443f-870e-6ad4b3dcd7c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c728289-406a-4c1e-b9c0-ed46fb6023a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b5b3c6-632e-47d9-bd24-3a10e88b974d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c728289-406a-4c1e-b9c0-ed46fb6023a0",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        },
        {
            "id": "c6d58823-7b12-4eec-8cda-09a8fc6123e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "069dc026-cce1-477e-bbed-00d061ec46c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d58823-7b12-4eec-8cda-09a8fc6123e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd9867b-1313-4cb4-929b-c35015c4d959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d58823-7b12-4eec-8cda-09a8fc6123e4",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        },
        {
            "id": "03dac640-7371-44e5-9e44-be3339276599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "758401a8-a740-4dd7-b414-04d7fc52deb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03dac640-7371-44e5-9e44-be3339276599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1359baee-efc9-43cb-937a-5b1bc65b6cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03dac640-7371-44e5-9e44-be3339276599",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        },
        {
            "id": "a2697251-97d9-4fcf-b196-ffee83b51144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "compositeImage": {
                "id": "52c223a2-7265-452c-9c7e-b0dda96f391d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2697251-97d9-4fcf-b196-ffee83b51144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a4594a-9e8a-4d6e-b400-41650a0d34ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2697251-97d9-4fcf-b196-ffee83b51144",
                    "LayerId": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e2b49e5-b15f-42dc-9bcd-65187e4b6dd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}