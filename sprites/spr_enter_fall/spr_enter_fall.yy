{
    "id": "19e8410c-81a4-4eba-83e5-3c3755dba972",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enter_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97279cb3-2d7e-41eb-9dd2-8b753cfd8d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19e8410c-81a4-4eba-83e5-3c3755dba972",
            "compositeImage": {
                "id": "3cf9cffe-0714-4830-9f3d-8f04a7f36cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97279cb3-2d7e-41eb-9dd2-8b753cfd8d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83fc0e1-67b5-4c0f-9dd9-2398e64913bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97279cb3-2d7e-41eb-9dd2-8b753cfd8d4b",
                    "LayerId": "e082651f-3508-4083-ac47-a09519ddef58"
                }
            ]
        },
        {
            "id": "9411dff1-2a39-4c58-a49f-42d52ba5e7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19e8410c-81a4-4eba-83e5-3c3755dba972",
            "compositeImage": {
                "id": "d31e0f96-3722-4933-af56-4c95e6536770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9411dff1-2a39-4c58-a49f-42d52ba5e7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03097ee9-2a47-4282-820a-51060c622823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9411dff1-2a39-4c58-a49f-42d52ba5e7fe",
                    "LayerId": "e082651f-3508-4083-ac47-a09519ddef58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e082651f-3508-4083-ac47-a09519ddef58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19e8410c-81a4-4eba-83e5-3c3755dba972",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}