{
    "id": "4b3216a8-b34c-4932-8c9a-f5cca08cfbec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Far_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7159a7b2-0c37-4ff0-81a6-ab9854f60ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b3216a8-b34c-4932-8c9a-f5cca08cfbec",
            "compositeImage": {
                "id": "7e7373ea-c602-4a38-b80c-02314b4c1444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7159a7b2-0c37-4ff0-81a6-ab9854f60ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847c1a7c-cc03-467d-8ead-efd8a63f34cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7159a7b2-0c37-4ff0-81a6-ab9854f60ced",
                    "LayerId": "83819e53-bf7d-4635-b1bd-db9e724fd15c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "83819e53-bf7d-4635-b1bd-db9e724fd15c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b3216a8-b34c-4932-8c9a-f5cca08cfbec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}