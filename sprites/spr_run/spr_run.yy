{
    "id": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 40,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "989d65ac-7cf6-4384-8245-878016dffc5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "641cc0b6-d738-4c9b-9766-a7c8dad9efcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989d65ac-7cf6-4384-8245-878016dffc5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7790abc-9f0f-4a95-bcd0-e3ad11a2206b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989d65ac-7cf6-4384-8245-878016dffc5a",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        },
        {
            "id": "fba0d7e7-bf51-4674-8060-dee52cbb210f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "262490ab-db30-40b5-902d-b4537b64ce57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba0d7e7-bf51-4674-8060-dee52cbb210f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16fff154-829e-4080-8466-b32ff44444f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba0d7e7-bf51-4674-8060-dee52cbb210f",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        },
        {
            "id": "2d82ec3a-2f1c-400d-942e-5389a2bbdcb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "e66815e1-ab57-4034-aaa2-b31441d9674a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d82ec3a-2f1c-400d-942e-5389a2bbdcb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ef701c-cb26-4a48-b611-a08cd48305e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d82ec3a-2f1c-400d-942e-5389a2bbdcb8",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        },
        {
            "id": "66183901-ea97-4bf8-a909-ef1edfb285ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "678a96e8-c73a-4e6b-886f-6d472323d89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66183901-ea97-4bf8-a909-ef1edfb285ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1214870f-b29e-42bd-888d-3609da669fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66183901-ea97-4bf8-a909-ef1edfb285ca",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        },
        {
            "id": "4e96dd98-4697-429d-a993-55f6b905085a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "7bca47aa-d895-4695-b639-e6e44f75b62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e96dd98-4697-429d-a993-55f6b905085a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d238c5e0-f3e9-4f71-b6cc-17054a9d4cab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e96dd98-4697-429d-a993-55f6b905085a",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        },
        {
            "id": "1bb03c0a-d806-4db9-bf37-247cb7b80dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "compositeImage": {
                "id": "d2d227ab-3b34-4f6c-8582-ba57acc00fab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb03c0a-d806-4db9-bf37-247cb7b80dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c54927-792e-4cfd-992e-d69c44e2cf7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb03c0a-d806-4db9-bf37-247cb7b80dca",
                    "LayerId": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ac428fbb-b92a-4b84-8ae5-5c97b67aa02a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3285d32-47d1-4821-8b8c-5f7c8c8b4113",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}