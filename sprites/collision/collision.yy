{
    "id": "75715540-93e2-49dc-a04b-7b467f6b0fa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 21,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "824cb422-cda6-419b-bbc1-e560303844a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75715540-93e2-49dc-a04b-7b467f6b0fa1",
            "compositeImage": {
                "id": "03cbe816-a8b0-463d-b024-eacb5a2f5b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "824cb422-cda6-419b-bbc1-e560303844a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266a531a-444e-43d8-a16f-74ddbb277630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "824cb422-cda6-419b-bbc1-e560303844a6",
                    "LayerId": "cc9a1a43-1dc7-4e50-b6de-7f889da57ac6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc9a1a43-1dc7-4e50-b6de-7f889da57ac6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75715540-93e2-49dc-a04b-7b467f6b0fa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}