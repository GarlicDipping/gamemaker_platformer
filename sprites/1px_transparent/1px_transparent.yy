{
    "id": "4e169b69-8dae-4398-bc65-f5050f0c00e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "1px_transparent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "233fb472-7f4a-4765-963c-db7beed2324b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e169b69-8dae-4398-bc65-f5050f0c00e4",
            "compositeImage": {
                "id": "9b1942a8-5820-438b-bb95-cbbc2fb1be6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233fb472-7f4a-4765-963c-db7beed2324b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1a2814-3b70-4194-b75d-f7fecf5ca79a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233fb472-7f4a-4765-963c-db7beed2324b",
                    "LayerId": "c86ac069-9bda-4c75-893f-a185dada2be9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "c86ac069-9bda-4c75-893f-a185dada2be9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e169b69-8dae-4398-bc65-f5050f0c00e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}