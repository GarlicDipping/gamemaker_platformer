{
    "id": "88f2b588-da20-49e8-b873-78b6dc2c378d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Prop_Meownya_Well",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 5,
    "bbox_right": 44,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "202d2d18-5b7a-4bb1-92bf-459da863364a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88f2b588-da20-49e8-b873-78b6dc2c378d",
            "compositeImage": {
                "id": "a852d382-9f68-442f-a067-50a8d9eac81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "202d2d18-5b7a-4bb1-92bf-459da863364a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54508eeb-3cfc-40af-b7dd-22c9c9b7ac5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "202d2d18-5b7a-4bb1-92bf-459da863364a",
                    "LayerId": "20bc4196-09e1-4938-b730-82716c281b4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "20bc4196-09e1-4938-b730-82716c281b4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88f2b588-da20-49e8-b873-78b6dc2c378d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}