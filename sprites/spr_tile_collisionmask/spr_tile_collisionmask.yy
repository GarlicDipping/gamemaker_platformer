{
    "id": "af646323-b91d-4ccc-86a6-dd7b7b3bec56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_collisionmask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0a82959c-23a0-49c4-94e4-7c8a0cf76b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af646323-b91d-4ccc-86a6-dd7b7b3bec56",
            "compositeImage": {
                "id": "74809c41-a967-4fd0-aa9b-777e58cbaa34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a82959c-23a0-49c4-94e4-7c8a0cf76b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef125289-06f2-4db5-b18d-ac21d65b58e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a82959c-23a0-49c4-94e4-7c8a0cf76b1d",
                    "LayerId": "76a9334f-f3a0-4589-915f-56e254bf6801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "76a9334f-f3a0-4589-915f-56e254bf6801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af646323-b91d-4ccc-86a6-dd7b7b3bec56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}