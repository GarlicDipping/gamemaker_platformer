{
    "id": "dbea3e1e-7980-4484-8fcd-e6ce2de18376",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Far_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "540ac324-7340-4ae1-8b82-0b31cc718860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbea3e1e-7980-4484-8fcd-e6ce2de18376",
            "compositeImage": {
                "id": "6a7fe7ad-55a9-4704-b97f-0b1ce1aeb322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "540ac324-7340-4ae1-8b82-0b31cc718860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b732ae69-d9e6-4196-a932-e60e4ce80532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "540ac324-7340-4ae1-8b82-0b31cc718860",
                    "LayerId": "a56b04f4-5866-44a7-91cb-e543df480c6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "a56b04f4-5866-44a7-91cb-e543df480c6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbea3e1e-7980-4484-8fcd-e6ce2de18376",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}