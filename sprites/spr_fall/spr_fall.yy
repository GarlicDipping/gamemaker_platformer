{
    "id": "5f092842-e2b5-4ea3-a34b-b4744bd7dca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a3a7d34-3beb-4bee-bdc5-b7a0c8f96050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f092842-e2b5-4ea3-a34b-b4744bd7dca9",
            "compositeImage": {
                "id": "468b5ba2-8db0-434d-bbd2-6545ff7f6748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3a7d34-3beb-4bee-bdc5-b7a0c8f96050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b84adf2-10fa-4ac4-a2ac-c3c3e51bc747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3a7d34-3beb-4bee-bdc5-b7a0c8f96050",
                    "LayerId": "e7320d06-a7c4-4821-b8cf-4acdef815697"
                }
            ]
        },
        {
            "id": "43cf26da-2828-46da-951a-87e8acd93658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f092842-e2b5-4ea3-a34b-b4744bd7dca9",
            "compositeImage": {
                "id": "cb3855eb-b26f-437a-be0e-fa9f903b727a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43cf26da-2828-46da-951a-87e8acd93658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19afd9af-37fa-46f7-b661-7ee9a7c98b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43cf26da-2828-46da-951a-87e8acd93658",
                    "LayerId": "e7320d06-a7c4-4821-b8cf-4acdef815697"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7320d06-a7c4-4821-b8cf-4acdef815697",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f092842-e2b5-4ea3-a34b-b4744bd7dca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}