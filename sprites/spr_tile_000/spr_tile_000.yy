{
    "id": "b6b20e91-7111-4d64-95e3-cba0333b7bc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_000",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d70b9bc9-4c71-43ae-90c0-8a223b5e37f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b20e91-7111-4d64-95e3-cba0333b7bc7",
            "compositeImage": {
                "id": "96953acb-a50f-446d-889b-e90794b300f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d70b9bc9-4c71-43ae-90c0-8a223b5e37f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d0e424-c96c-46ae-b047-f35bc64036a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d70b9bc9-4c71-43ae-90c0-8a223b5e37f8",
                    "LayerId": "339f17b5-65da-4e7c-90d2-6577567efbca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "339f17b5-65da-4e7c-90d2-6577567efbca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6b20e91-7111-4d64-95e3-cba0333b7bc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}