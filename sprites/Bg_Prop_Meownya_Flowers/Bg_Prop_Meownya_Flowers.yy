{
    "id": "b934c443-237c-48e6-9448-5af785ac5f97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Prop_Meownya_Flowers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 76,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2e2dea92-d64c-4633-8c7e-8bbd7eda3604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b934c443-237c-48e6-9448-5af785ac5f97",
            "compositeImage": {
                "id": "199ba7de-30b2-4e37-885a-84bd1d0af818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e2dea92-d64c-4633-8c7e-8bbd7eda3604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed321c75-12b9-4299-8e5d-8b674da08adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e2dea92-d64c-4633-8c7e-8bbd7eda3604",
                    "LayerId": "7faaa2e0-ebd8-4f34-89b6-6832921e2114"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7faaa2e0-ebd8-4f34-89b6-6832921e2114",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b934c443-237c-48e6-9448-5af785ac5f97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}