{
    "id": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_land",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 39,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a74b7e47-7cd5-4870-b814-510ce334dda7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "compositeImage": {
                "id": "ecc8f6b0-ccb0-4f5f-9528-7066fd61fa17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74b7e47-7cd5-4870-b814-510ce334dda7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4789591c-288d-45a8-bc4e-42ade55ef232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74b7e47-7cd5-4870-b814-510ce334dda7",
                    "LayerId": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557"
                }
            ]
        },
        {
            "id": "6429ef3e-3f55-477c-9cf1-491f1e068d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "compositeImage": {
                "id": "1216f348-f7e3-416d-9772-5ffe5c7d9e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6429ef3e-3f55-477c-9cf1-491f1e068d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40599cdb-4379-4164-b99a-dc0a5ee746f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6429ef3e-3f55-477c-9cf1-491f1e068d27",
                    "LayerId": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557"
                }
            ]
        },
        {
            "id": "dff461e8-3a4c-4502-8eb1-dd8e59ed792d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "compositeImage": {
                "id": "3fba0ce0-64e8-4878-96c0-654a4acd4135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff461e8-3a4c-4502-8eb1-dd8e59ed792d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "222d03c6-2048-4beb-bb27-0bdde1900265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff461e8-3a4c-4502-8eb1-dd8e59ed792d",
                    "LayerId": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557"
                }
            ]
        },
        {
            "id": "9757474b-9502-4f28-bbd9-bb13b2394a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "compositeImage": {
                "id": "bc341ffa-4c79-4eaa-9018-37ddeeebac10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9757474b-9502-4f28-bbd9-bb13b2394a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e56333-5449-425a-90d7-cc49f2a00ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9757474b-9502-4f28-bbd9-bb13b2394a38",
                    "LayerId": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557"
                }
            ]
        },
        {
            "id": "8efe66ae-a718-4444-be78-9e16b05d94fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "compositeImage": {
                "id": "61e6e78d-069e-4cff-8517-9b14f71aa550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8efe66ae-a718-4444-be78-9e16b05d94fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9083deef-ecf5-4a56-b762-c335c3b08c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8efe66ae-a718-4444-be78-9e16b05d94fe",
                    "LayerId": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fbdf4aed-aa54-4866-9f5a-f3dceb0bd557",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0da0f5f0-bdc9-42a9-beea-80d0a09a7226",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}