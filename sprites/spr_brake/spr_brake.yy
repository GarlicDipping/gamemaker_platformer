{
    "id": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 24,
    "bbox_right": 41,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c66c738f-850c-4742-bd71-0ede12afc774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "compositeImage": {
                "id": "6bd61a19-3498-4862-bc23-64f17744f7c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66c738f-850c-4742-bd71-0ede12afc774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41cce0a-c810-49a7-9808-5a19eb607fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66c738f-850c-4742-bd71-0ede12afc774",
                    "LayerId": "8945f605-641e-4acc-bdc0-9ac7ee32c572"
                }
            ]
        },
        {
            "id": "66a69662-d88d-4625-9891-f513ebb75f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "compositeImage": {
                "id": "201a65af-60c2-46a2-a06b-138240656f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a69662-d88d-4625-9891-f513ebb75f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d560936b-1e35-419e-abae-f6aa757902d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a69662-d88d-4625-9891-f513ebb75f11",
                    "LayerId": "8945f605-641e-4acc-bdc0-9ac7ee32c572"
                }
            ]
        },
        {
            "id": "aa139736-7d54-4346-9b3c-1be3c5db503e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "compositeImage": {
                "id": "4aeea041-1a29-4875-b90a-15b5ecb352d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa139736-7d54-4346-9b3c-1be3c5db503e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56aa250d-22fe-4614-9b2a-776e422fbfff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa139736-7d54-4346-9b3c-1be3c5db503e",
                    "LayerId": "8945f605-641e-4acc-bdc0-9ac7ee32c572"
                }
            ]
        },
        {
            "id": "656283d4-a654-420e-940b-711a66b889f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "compositeImage": {
                "id": "e06b8ca9-f0f0-4c8a-8d4a-d3a14f66df8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656283d4-a654-420e-940b-711a66b889f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e33863-f482-4569-85b5-4278edaedbbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656283d4-a654-420e-940b-711a66b889f8",
                    "LayerId": "8945f605-641e-4acc-bdc0-9ac7ee32c572"
                }
            ]
        },
        {
            "id": "13a78af8-7d97-43d4-b6c0-7c9c89743db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "compositeImage": {
                "id": "7eae026e-1044-4e74-9ea4-a8f882f2cc6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a78af8-7d97-43d4-b6c0-7c9c89743db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee83cbb-2405-49ec-ae9b-b3cf8eca7ef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a78af8-7d97-43d4-b6c0-7c9c89743db6",
                    "LayerId": "8945f605-641e-4acc-bdc0-9ac7ee32c572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8945f605-641e-4acc-bdc0-9ac7ee32c572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9cb90d1-8ba7-4984-af2c-768b578c5d36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}