{
    "id": "7eb5769a-2bac-4eaa-a103-8287a57b237d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Meownya_House",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 157,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2e343cbd-f25f-47ef-ba7c-858d84f508a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb5769a-2bac-4eaa-a103-8287a57b237d",
            "compositeImage": {
                "id": "926be20d-8e85-47f8-b2e7-5647b3b89d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e343cbd-f25f-47ef-ba7c-858d84f508a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37a7052-f26d-4952-b182-666976fc4440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e343cbd-f25f-47ef-ba7c-858d84f508a5",
                    "LayerId": "9d735224-6dc6-4b01-ba42-1b30a880ec80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9d735224-6dc6-4b01-ba42-1b30a880ec80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eb5769a-2bac-4eaa-a103-8287a57b237d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}