{
    "id": "5b2274ec-71aa-4199-a80a-e5b897bbc203",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bg_Prop_Meownya_Flowers_wooltari",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 79,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1df9e5a9-d613-4916-8ed2-926a5219081c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b2274ec-71aa-4199-a80a-e5b897bbc203",
            "compositeImage": {
                "id": "d2a968ac-433b-423e-8cff-8e50469e5157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df9e5a9-d613-4916-8ed2-926a5219081c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123090d0-933b-47de-8042-b51195cf2ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df9e5a9-d613-4916-8ed2-926a5219081c",
                    "LayerId": "f1244f83-bec2-4934-802d-deac8ecaa4b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f1244f83-bec2-4934-802d-deac8ecaa4b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b2274ec-71aa-4199-a80a-e5b897bbc203",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}