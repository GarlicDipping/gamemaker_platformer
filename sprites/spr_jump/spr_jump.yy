{
    "id": "af6b4fbf-02c7-408c-8d92-b5dd56f44842",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 38,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19007004-62f1-42db-a307-0e0ee03375cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af6b4fbf-02c7-408c-8d92-b5dd56f44842",
            "compositeImage": {
                "id": "3151dbbe-f2f5-4ed3-a67d-abc12b6965b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19007004-62f1-42db-a307-0e0ee03375cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20414313-a023-4aa2-a2fe-cdd09f9efc5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19007004-62f1-42db-a307-0e0ee03375cf",
                    "LayerId": "eb5876e4-ee8b-488d-847d-bf346bc16466"
                }
            ]
        },
        {
            "id": "4cbef449-4809-401e-b063-9a1f5036a765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af6b4fbf-02c7-408c-8d92-b5dd56f44842",
            "compositeImage": {
                "id": "1d95cc4b-6f3e-4eb3-82cc-a561589ad626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cbef449-4809-401e-b063-9a1f5036a765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13ac64d-4f9e-45f9-ab0d-45b7a25217fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cbef449-4809-401e-b063-9a1f5036a765",
                    "LayerId": "eb5876e4-ee8b-488d-847d-bf346bc16466"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eb5876e4-ee8b-488d-847d-bf346bc16466",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af6b4fbf-02c7-408c-8d92-b5dd56f44842",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}