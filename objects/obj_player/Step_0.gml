/// @description Player Step Event
key_right = keyboard_check(vk_right);
key_left = keyboard_check(vk_left);
key_jump = keyboard_check(vk_space);
key_jump_up = keyboard_check_released(vk_space);



if(key_right)
{
	if(current_state == player_states.idle)
	{
		run_frame_count = 0;
		current_state = player_states.run;
		image_index = 0;
		sprite_index = spr_run;
	}
	
	if(canMove)
	{
		//if(current_state == player_states.land)
		//{
		//	hSpeed = 0;
		//	return;
		//}
		hSpeed = hmove_speed;
		//}
		look_direction = 1;
		image_xscale = 1;
		run_frame_count++;
	}	
}
else if(key_left)
{	
	if(current_state == player_states.idle)
	{
		run_frame_count = 0;
		current_state = player_states.run;
		image_index = 0;
		sprite_index = spr_run;
	}
	if(canMove)
	{
		//if(current_state == player_states.land)
		//{
		//	hSpeed = 0;
		//	return;
		//}
		hSpeed = -hmove_speed;

		look_direction = -1;
		image_xscale = -1;
		run_frame_count++;
	}
}
else
{
	if(current_state == player_states.run)
	{
		current_state = player_states.idle;
		image_index = 0;
		if(run_frame_count >= 8)
		{
			sprite_index = spr_brake;
		}
		else
		{
			sprite_index = spr_idle;
		}
	}
	hSpeed = 0;
}

if(key_jump && on_ground == true)
{
	current_state = player_states.jump;
	image_index = 0;
	sprite_index = spr_jump;
		
	on_ground = false;
	vSpeed = jump_up_speed_max;
}
else if(key_jump_up)
{
	if(vSpeed < 0)
	{
		vSpeed *= 0.25;
	}
}

if(current_state == player_states.jump)
{
	if(vSpeed >= 0)
	{
		current_state = player_states.fall;
		image_index = 0;
		sprite_index = spr_enter_fall;
	}
}

vSpeed = clamp(vSpeed, jump_up_speed_max, jump_down_speed_max);

x += hSpeed;
y += vSpeed;

//중력 적용.
vSpeed += .2;

var collides_lb = scr_check_collision(bbox_left, bbox_bottom);
var collides_rb = scr_check_collision(bbox_right, bbox_bottom);
if(collides_lb[2] != 0 || collides_rb[2] != 0)
{
	if(on_ground == false && current_state == player_states.fall)
	{
		if(key_left || key_right)
		{
			sprite_index = spr_run;
			current_state = player_states.run;
		}
		else
		{
			sprite_index = spr_idle;
			current_state = player_states.idle;
		}		
		image_index = 0;
		run_frame_count = 0;
		hSpeed = 0;
	}
	y = collides_lb[1];
	//vSpeed = 0;
	on_ground = true;
}
else
{
	if(on_ground == true)
	{
		current_state = player_states.fall;
		image_index = 0;
		sprite_index = spr_fall;
	}
	on_ground = false;
	//falling	
}
//show_debug_message("bbox_left : " + string(bbox_left) + ", bbox_right : " + string(bbox_right) + ", bbox_bottom : " + string(bbox_bottom) + ", collides_lb : " + string(collides_lb) + ", rb : " + string(collides_rb));