{
    "id": "aaf0eb08-22e4-4b5c-8a5c-008e79d35e5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "2700a5aa-2f1c-4457-a9bf-5605623a2df8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aaf0eb08-22e4-4b5c-8a5c-008e79d35e5f"
        },
        {
            "id": "4b8b1a2a-2dc4-434e-9fb7-b1d949eb7ef7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aaf0eb08-22e4-4b5c-8a5c-008e79d35e5f"
        },
        {
            "id": "cbf6f72c-8de0-4663-a1d6-a1c79a02044a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "aaf0eb08-22e4-4b5c-8a5c-008e79d35e5f"
        }
    ],
    "maskSpriteId": "75715540-93e2-49dc-a04b-7b467f6b0fa1",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70bffa18-871f-4671-8f27-e737ca63e638",
    "visible": true
}