{
    "id": "87cd7f1a-89ff-4f6e-b90c-b8f42496da41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameController",
    "eventList": [
        {
            "id": "0c6015ea-4db5-4dfe-87c4-732918402c1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87cd7f1a-89ff-4f6e-b90c-b8f42496da41"
        },
        {
            "id": "4c9bae1a-9d47-454f-b93f-cf9c0134b386",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87cd7f1a-89ff-4f6e-b90c-b8f42496da41"
        },
        {
            "id": "e4a2eda1-9765-4a74-9ea6-e64e2b82c49f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "87cd7f1a-89ff-4f6e-b90c-b8f42496da41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e169b69-8dae-4398-bc65-f5050f0c00e4",
    "visible": true
}