{
    "id": "15ffbeff-bbe9-4432-9230-011345b2bfcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Bg_Prop_Flowers_Wooltari",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "5b2274ec-71aa-4199-a80a-e5b897bbc203",
    "visible": true
}