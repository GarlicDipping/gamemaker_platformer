///scr_cam_get_raw_target_position(camera)

var player_direction = obj_player.look_direction;

var cam_width = camera_get_view_width(argument[0]);
var cam_height = camera_get_view_height(argument[0]);

var cam_current_x = camera_get_view_x(argument[0]);
var cam_current_y = camera_get_view_y(argument[0]);
//플레이어가 보는 방향으로 32px만큼 카메라를 더 이동
var cam_actual_target_x = obj_player.x - cam_width / 2 + player_direction * 16;
cam_actual_target_x = clamp( cam_actual_target_x, 0, room_width - cam_width);
		
var cam_actual_target_y = obj_player.y - cam_height / 2;
cam_actual_target_y = clamp( cam_actual_target_y, 0, room_height - cam_height);	

var cam_target_pos;
cam_target_pos[0] = round(cam_actual_target_x);
cam_target_pos[1] = round(cam_actual_target_y);
 
return cam_target_pos;