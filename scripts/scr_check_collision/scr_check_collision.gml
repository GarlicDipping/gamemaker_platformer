///scr_check_collision(target_x, target_y)


//var room_meter_w = room_width / 16;
//var room_meter_h = room_height / 16;
//show_debug_message("room meter : " + string(room_meter_w) + ", " + string(room_meter_h));

//var lay_id = layer_get_id("Collision");
//var map_id = layer_tilemap_get_id(lay_id);

//for(var vy = 0; vy < room_meter_h; vy++)
//{
//	var line = "";
//	for(var vx = 0; vx < room_meter_w; vx++)
//	{
//		var d = tilemap_get(map_id, vx, vy);
//		line += string(d);
//	}
//	line += "\n";
//	show_debug_message(line);
//}


var lay_id = layer_get_id("Collision");
var map_id = layer_tilemap_get_id(lay_id);
var check_x = floor(argument[0] / 16);
var check_y = floor(argument[1] / 16);

var tile_at_target = tilemap_get(map_id, check_x, check_y);

//Tile Position & Type
var collision_info;
collision_info[0] = check_x * 16;
collision_info[1] = check_y * 16;
collision_info[2] = tile_at_target;

return collision_info;