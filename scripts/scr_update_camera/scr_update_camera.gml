///scr_update_camera(camera);
var cam_raw_target = scr_cam_get_raw_target_position(argument[0]);

var cam_current_x = camera_get_view_x(argument[0]);
var cam_current_y = camera_get_view_y(argument[0]);

var cam_target_x = lerp(cam_current_x, cam_raw_target[0], 0.1);
var cam_target_y = lerp(cam_current_y, cam_raw_target[1], 0.1);

with(CameraController)
{
	xRemainder += (cam_target_x - cam_current_x);
	yRemainder += (cam_target_y - cam_current_y);
	
	xRemainder = clamp(xRemainder, -3, 3);
	yRemainder = clamp(yRemainder, -3, 3);

	var snapped_target_x = cam_current_x + round(xRemainder);
	var snapped_target_y = cam_current_y + round(yRemainder);
	
	camera_set_view_pos(argument[0], snapped_target_x, snapped_target_y);
	
	xRemainder -= round(xRemainder);
	yRemainder -= round(yRemainder);
	
	if(abs(snapped_target_x - cam_raw_target[0]) < 1 
		&& abs(snapped_target_y - cam_raw_target[1]) < 1)
	{
		xRemainder = 0;
		yRemainder = 0;
		//if(obj_player.vSpeed == 0 && obj_player.hSpeed == 0)
		//{
		//	do_handheld_shake = true;
		//	handheld_initial_position[0] = snapped_target_x;
		//	handheld_initial_position[1] = snapped_target_y;
			
		//	var rdx = irandom_range(-handheld_range_px, handheld_range_px);
		//	var rdy = irandom_range(-handheld_range_px, handheld_range_px);
						
		//	handheld_target_position[0] = handheld_initial_position[0] + rdx;
		//	handheld_target_position[1] = handheld_initial_position[1] + rdy;
			
		//	var cam_width = camera_get_view_width(argument[0]);
		//	var cam_height = camera_get_view_height(argument[0]);

		//	handheld_target_position[0] = clamp( handheld_target_position[0], 0, room_width - cam_width);
		//	handheld_target_position[1] = clamp( handheld_target_position[1], 0, room_height - cam_height);	
		//}
	}
	
	
	//show_debug_message("xR : " + string(xRemainder) + ", yR : " + string(yRemainder)
	//+ "/ snapped : " + string(snapped_target_x) + ", " + string(snapped_target_y)
	//+ "/ cam_raw_target : " + string(cam_raw_target[0]) + ", " + string(cam_raw_target[1]));
}