///scr_update_handheld_camera(camera);
with(CameraController)
{
	var cam_current_x = camera_get_view_x(argument[0]);
	var cam_current_y = camera_get_view_y(argument[0]);
	
	var target_x = lerp(cam_current_x, handheld_target_position[0], 0.04);
	var target_y = lerp(cam_current_y, handheld_target_position[1], 0.04);

	xRemainder += (target_x - cam_current_x);
	yRemainder += (target_y - cam_current_y);

	var snapped_target_x = cam_current_x + round(xRemainder);
	var snapped_target_y = cam_current_y + round(yRemainder);

	camera_set_view_pos(argument[0], snapped_target_x, snapped_target_y);
	
	xRemainder -= round(xRemainder);
	yRemainder -= round(yRemainder);
	
	if(abs(snapped_target_x - handheld_target_position[0]) < 1 
		&& abs(snapped_target_y - handheld_target_position[1]) < 1)
	{
		xRemainder = 0;
		yRemainder = 0;
		if(obj_player.vSpeed == 0 && obj_player.hSpeed == 0)
		{
			handheld_stay_frame_count += 1;
			if(handheld_stay_frame_count > handheld_stay_frame)
			{
				var cam_width = camera_get_view_width(argument[0]);
				var cam_height = camera_get_view_height(argument[0]);

				var rdx = irandom_range(-handheld_range_px, handheld_range_px);
				var rdy = irandom_range(-handheld_range_px, handheld_range_px);
						
				handheld_target_position[0] = handheld_initial_position[0] + rdx;
				handheld_target_position[1] = handheld_initial_position[1] + rdy;				
				
				handheld_target_position[0] = clamp( handheld_target_position[0], 0, room_width - cam_width);
				handheld_target_position[1] = clamp( handheld_target_position[1], 0, room_height - cam_height);	
			
				handheld_stay_frame_count = 0;
				show_debug_message("refresh handheld");
			}			
		}
	}	
	
	if(obj_player.vSpeed != 0 || obj_player.hSpeed != 0)
	{
		do_handheld_shake = false;
	}
	//show_debug_message("xR : " + string(xRemainder) + ", yR : " + string(yRemainder)
	//+ "/ snapped : " + string(snapped_target_x) + ", " + string(snapped_target_y)
	//+ "/ cam_raw_target : " + string(cam_raw_target[0]) + ", " + string(cam_raw_target[1]));
}